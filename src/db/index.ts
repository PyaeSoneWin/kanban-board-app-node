import { Pool } from "pg";
const pool: Pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: 5432,
});
export default {
  select: (text: string) => pool.query(text),
  query: (text: string, params: Array<string>) => pool.query(text, params),
}