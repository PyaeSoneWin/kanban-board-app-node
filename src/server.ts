import http from "http";
import express from "express";
import dotenv from "dotenv";
dotenv.config();
import { applyMiddleware } from "./utils";
import middleware from "./middleware";
import mountRoutes from "./routes";

const { PORT = 3000 } = process.env;

// const 
const router = express();
applyMiddleware(middleware, router);
mountRoutes(router);

const server = http.createServer(router);

server.listen(PORT, () =>
  console.log(`Server is running http://localhost:${PORT}...`)
);
