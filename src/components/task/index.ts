import Router from 'express-promise-router';
import db from '../../db';
const router = Router();

router.get('/', async (req,res) => {
  const { rows } = await db.select('SELECT * FROM task');
  res.send(rows);
});

router.post('/new', async (req, res) => {
  const { title, description } = req.body;
  const result = await db.query('INSERT INTO task (title, description, user_id) VALUES ($1, $2, 1)', [title, description]);
  res.status(201).send(result);
});

router.put('/:id', async (req, res) => {
  const { id } = req.params;
  const { title, description } = req.body;
  const result = await db.query(
    'UPDATE task SET title = $1, description = $2 WHERE id = $3',
    [title, description, id]
  );
  res.status(201).send(result);
});

router.delete('/:id', async (req, res) => {
  const { id } = req.params;
  const result = await db.query('DELETE FROM task WHERE id = $1', [id]);
  res.status(201).send(result);
});

export default {
  router
};