import Router from 'express-promise-router';
import db from '../../db';
const router = Router();

router.get('/', async (req,res) => {
  const { rows } = await db.select('SELECT * FROM fb_user');
  res.send(rows);
});

router.post('/signIn', async (req, res) => {
  const { name, profile, userID } = req.body;
  const { rows } = await db.query('SELECT * FROM fb_user WHERE userid = $1', [userID]);
  if(rows[0]) {
    const result = await db.query(
      'UPDATE fb_user SET name = $1, profile = $2 WHERE userid = $3', 
      [name, profile, userID]
    );
    res.status(201).send(result);
  } else {
    const result = await db.query(
      'INSERT INTO fb_user (name, profile, userid) VALUES ($1, $2, $3)', 
      [name, profile, userID]
    );
    res.status(201).send(result);
  }
});

export default {
  router
};