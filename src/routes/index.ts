import task from '../components/task';
import user from '../components/user';

export default (app: any) => {
  app.use('/tasks', task.router);
  app.use('/users', user.router);
};